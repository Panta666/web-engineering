package webengShop.presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.Response;

import webengShop.businesslogic.ArticleManager;
import webengShop.businesslogic.ShoppingcartManager;
import webengShop.transfer.Article;

/**
 * Servlet implementation class Servlet
 */
@WebServlet(urlPatterns ={ "/Frontcontroller" })
public class Frontcontroller extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ArticleManager articleManager;
    private ShoppingcartManager shoppingcartManager;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Frontcontroller() {
        super();
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        HttpSession sess = request.getSession(false);

        if (getServletContext().getAttribute("articleManagement") == null) {
            articleManager = new ArticleManager();
            getServletContext().setAttribute("articleManagement", articleManager);
        } else {
            articleManager = (ArticleManager) getServletContext().getAttribute("articleManagement");
        }

        if (sess == null) {
            sess = request.getSession(true);
            shoppingcartManager = new ShoppingcartManager();
            sess.setAttribute("shoppingcart", shoppingcartManager);
        } else {
            sess.getAttribute("shoppingcart");
        }

        

        String action = request.getParameter("action");
        action = (action == null)? "" : action;

        String target = "Frontcontroller";
        switch(action) {
            case "admin":
                target = processAdmin(request, sess);
                break;
            case "update":
                target = processUpdate(request, sess);
                break;
            case "add":
                target = processAdd(request, sess);
                break;
            case "articlelist":
                target = processArticlelist(request);
                break;
            case "articledetails":
                target = processArticledetails(request);
                break;
            case "shoppingcart":
                target = processShoppingcart(request);
                break;
            case "checkout":
                target = processCheckout();
                break;
            default:
                target = "Frontcontroller";
                break;
        }
        printHead(out);
        RequestDispatcher rd = request.getRequestDispatcher(target);
        printMenu(out);

        if (target == "Frontcontroller" || target == "") {
            out.println("Sie haben keine Auswahl getroffen.");
        } else {
            rd.include(request, response);
            if(request.getAttribute("error") != null) {
                response.sendError((Integer) request.getAttribute("error"));
            }
        }
        printFoot(out);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Implemented destroy for clean closing of the Server.
	 * Calls the super.destory().
	 */
	@Override
	public void destroy() {
		super.destroy();
    }

    private String processUpdate(HttpServletRequest request, HttpSession sess) {
        ArrayList<String> errorlist = new ArrayList<String>();
        try {
            int origID = Integer.valueOf(request.getParameter("origid"));
            int id = Integer.valueOf(request.getParameter("id"));
            String name = request.getParameter("name");
            float price = Float.valueOf(request.getParameter("price"));
            int quantity = Integer.valueOf(request.getParameter("quantity"));

            Article a = articleManager.getArticle(origID);

            if(request.getParameter("sub").equals("Entfernen")) {
                articleManager.delteArticle(a);
            } else {
                a.setId(id);
                a.setName(name);
                a.setPrice(price);
                a.setQuantity(quantity);
                errorlist = articleManager.updateArticle(a);
                if(!errorlist.isEmpty()) {
                    request.setAttribute("errorlist", errorlist);
                }
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            if(request.getParameter("id") == null) errorlist.add("ID Fehlerhaft.");
            if(request.getParameter("price") == null) errorlist.add("Preis Fehlerhaft.");
            if(request.getParameter("quantity") == null) errorlist.add("Anzahl Fehlerhaft.");
            request.setAttribute("errorlist", errorlist);
        }
        return processAdmin(request, sess);
        //return "";
    }

    private String processCheckout() {
        shoppingcartManager.checkout();
        return "thanks.html";
    }

    private String processShoppingcart(HttpServletRequest request) {
        String target = "shoppingcart.jsp";
        if(request.getParameter("id") != null) {
            int quantity = Integer.valueOf(request.getParameter("quantity"));
            int quantityLeft = articleManager.getArticle(Integer.valueOf(request.getParameter("id"))).getQuantity();
            Article a = articleManager.takeArticle(Integer.valueOf(request.getParameter("id")), quantity);
            if(quantity <= quantityLeft && a != null) {
                for(int i = 0; i < quantity; i++) {
                    shoppingcartManager.addArticle(a);
                }
            } else {
                System.out.println("Quantity too high.");
            }
        }

        request.setAttribute("shoppingcartlist", shoppingcartManager.getArticlelist());
        request.setAttribute("shoppingcartSum", shoppingcartManager.getShoppingcartsum());
        
        return target;
    }

    private String processAdmin(HttpServletRequest request, HttpSession sess) {

        //sess.setAttribute("AUTHTOKEN", "totalSicher");
        request.setAttribute("alist", articleManager.getArticleList());
        return "admin.jsp";
    }

    private String processAdd(HttpServletRequest request, HttpSession sess) {
        ArrayList<String> errorlist = new ArrayList<String>();
        try {
            int id = Integer.valueOf(request.getParameter("id"));
            String name = request.getParameter("name");
            float price = Float.valueOf(request.getParameter("price"));
            int quantity = Integer.valueOf(request.getParameter("quantity"));

            Article a = new Article(id, name, price, quantity);

            errorlist = articleManager.newArticle(a);
            System.out.println(errorlist.size());
            if(!errorlist.isEmpty()) {
                request.setAttribute("errorlist", errorlist);
            }

        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            if(request.getParameter("id") == null) errorlist.add("ID Fehlerhaft.");
            if(request.getParameter("price") == null) errorlist.add("Preis Fehlerhaft.");
            if(request.getParameter("quantity") == null) errorlist.add("Anzahl Fehlerhaft.");
            request.setAttribute("errorlist", errorlist);
        }
        return processAdmin(request, sess);

    }

    private String processArticlelist(HttpServletRequest request) {
        request.setAttribute("alist", articleManager.getArticleList());
        return "articlelist.jsp";
    }

    private String processArticledetails(HttpServletRequest request) {
        String target = "";
        String id = request.getParameter("ID");
                if(id != null) {
                    request.setAttribute("article", articleManager.getArticle(Integer.valueOf(id)));
                    target = "articledetails.jsp";
                }
        return target;
    }
    
    
    private void printHead(PrintWriter out) {
        out.println("<!DOCTYPE html><html><head><title>Auswahlseite</title></head><body>");
    }

    private void printFoot(PrintWriter out) {
        out.println("</body></html>");
    }

    private void printMenu(PrintWriter out) {
        out.println("<ul>");
        out.println("<li><a href='./Frontcontroller?action=admin&AUTHTOKEN=totalSicher'>Admin</a></li>");
        out.println("<li><a href='./Frontcontroller?action=articlelist'>Artikelliste</a></li>");
        out.println("<li><a href='./Frontcontroller?action=shoppingcart'>Warenkorb</a></li>");
        out.println("</ul>");
    }

}
