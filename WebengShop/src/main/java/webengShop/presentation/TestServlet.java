package webengShop.presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import webengShop.businesslogic.ArticleManager;
import webengShop.businesslogic.ShoppingcartManager;
import webengShop.transfer.Article;

/**
 * Servlet implementation class Servlet
 */
@WebServlet(urlPatterns ={ "/TestServlet" })
public class TestServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private ShoppingcartManager shoppingcartManager = new ShoppingcartManager();
    private ArticleManager articleManager = new ArticleManager();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
      
    }

    public void init() throws ServletException {
        super.init();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html><html><head></head><body>");

        out.println("<h1>Praktikum 4 - Shopsystem mit 3-Schicht-Architektur</h1></br></br></br>");
        
        out.println("           <b>Artikelliste ausgeben.</b>");
        generateArticleList(out);
        
        out.println("           </br></br><b>F&uumlge 3 Artikel zum Warenkorb hinzu.</b></br>");
        addThreeArticle();
        
        out.println("           </br></br><b>Warenkorb ausgeben.</b>");
        generateShoppingcartlist(out);

        out.println("           </br></br><b>Artikelliste ausgeben nachdem Waren in den Warenkorb gelegt wurden.</b>");
        generateArticleList(out);

        out.println("           </br></br><b>F&aumlllige Summe.</b>");
        double toPay = shoppingcartManager.checkout();
        showSum(out, toPay);
        
        out.println("</body></html>");
    }
    
    private void showSum(PrintWriter out, double sum) {
        out.println("           </br>Bitte bezahlen sie <u>" + sum + "&euro;</u>");
    }

    private void addThreeArticle() {
        shoppingcartManager.addArticle(articleManager.takeArticle(1, 1));
        shoppingcartManager.addArticle(articleManager.takeArticle(1, 1));
        shoppingcartManager.addArticle(articleManager.takeArticle(3, 1));
    }

    private void generateShoppingcartlist(PrintWriter out) {
        HashMap<Article, Integer> shoppingcartlist = shoppingcartManager.getArticlelist();
        double summe = shoppingcartManager.getShoppingcartsum();


        out.println("</br>");
        out.println("       <table>");
        out.println("           <tr>");
        out.println("               <th>");
        out.println("                   ID");
        out.println("               </th>");
        out.println("               <th>");
        out.println("                   Name");
        out.println("               </th>");
        out.println("               <th>");
        out.println("                   Preis");
        out.println("               </th>");
        out.println("               <th>");
        out.println("                   Menge");
        out.println("               </th>");
        out.println("           </tr>");
        for(Article a : shoppingcartlist.keySet()) {
            out.println("           <tr>");
            out.println("               <td>");
            out.println("                   "+a.getId());
            out.println("               </td>");
            out.println("               <td>");
            out.println("                   " + a.getName());
            out.println("               </td>");
            out.println("               <td>");
            out.println("                   " + a.getPrice() + "&euro;");
            out.println("               </td>");
            out.println("               <td>");
            out.println("                   " + shoppingcartlist.get(a));
            out.println("               </td>");
            out.println("           </tr>");
        }
        out.println("               <tr>");
        out.println("                   <td colspan= '4' style='text-align:right'>");
        out.println("                        <b><u>Summe: " + summe + "&euro;</u></b>");
        out.println("                   </td>");
        out.println("               </tr>");
        out.println("</table>");
    }

	private void generateArticleList(PrintWriter out) {
        List<Article> articlelist = articleManager.getArticleList();
        out.println("</br>");
        out.println("       <table>");
        out.println("           <tr>");
        out.println("               <th>");
        out.println("                   ID");
        out.println("               </th>");
        out.println("               <th>");
        out.println("                   Name");
        out.println("               </th>");
        out.println("               <th>");
        out.println("                   Preis");
        out.println("               </th>");
        out.println("               <th>");
        out.println("                   Menge");
        out.println("               </th>");
        out.println("           </tr>");
        for(Article a : articlelist) {
            out.println("           <tr>");
            out.println("               <td>");
            out.println("                   "+a.getId());
            out.println("               </td>");
            out.println("               <td>");
            out.println("                   " + a.getName());
            out.println("               </td>");
            out.println("               <td>");
            out.println("                   " + a.getPrice());
            out.println("               </td>");
            out.println("               <td>");
            out.println("                   " + a.getQuantity());
            out.println("               </td>");
            out.println("           </tr>");
        }
        out.println("</table>");

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Implemented destroy for clean closing of the Server.
	 * Calls the super.destory().
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

}
