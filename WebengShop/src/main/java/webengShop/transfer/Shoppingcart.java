package webengShop.transfer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Shoppingcart manages the Items that are put inside a Shoppingcart.
 */
public class Shoppingcart implements Serializable {
    
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private HashMap<Article, Integer> articlelist;

    /**
     * Constructor initializes an empty shoppingcart.
     */
    public Shoppingcart(){
        articlelist = new HashMap<Article, Integer>();
    }

    public HashMap<Article, Integer> getArticlelist() {
        return articlelist;
    }

    public void addArticle(Article article) {
            int ammount = 1;
            if( articlelist.containsKey(article)) {
                ammount += articlelist.get(article);
                articlelist.replace(article, ammount);
            } else {
                articlelist.put(article, ammount);
            }
    }

    public double checkout() {
        double toPay = getShoppingcartsum();
        articlelist.clear();
        return toPay;
    }

    public HashMap<Article, Integer> cancel(){
        HashMap<Article, Integer> article = articlelist;
        articlelist.clear();
        return article;
    }

    public float getShoppingcartsum(){
        float summe = 0;
        for (Map.Entry<Article, Integer> article: articlelist.entrySet()) {
            summe += article.getKey().getPrice() * article.getValue();
        }
        
        return summe;
    }

}