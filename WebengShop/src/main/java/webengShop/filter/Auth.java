package webengShop.filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "Auth", urlPatterns = "/admin.jsp" , dispatcherTypes = {DispatcherType.INCLUDE, DispatcherType.REQUEST})
public class Auth implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
                //HttpSession session = ((HttpServletRequest) request).getSession(false);

                //if(session != null && session.getAttribute("AUTHTOKEN").equals(request.getServletContext().getInitParameter("AUTHTOKEN"))){
                if(request.getParameter("AUTHTOKEN") != null && request.getParameter("AUTHTOKEN").equals(request.getServletContext().getInitParameter("AUTHTOKEN"))) {
                    chain.doFilter(request, response);
                    System.out.println("Filter Pass");
                }
                else {
                    request.setAttribute("error", HttpServletResponse.SC_FORBIDDEN);
                    ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                    System.out.println("Filter deny");
                }

    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }
    
}