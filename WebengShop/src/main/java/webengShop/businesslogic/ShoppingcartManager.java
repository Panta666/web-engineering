package webengShop.businesslogic;

import java.util.HashMap;

import webengShop.transfer.Article;
import webengShop.transfer.Shoppingcart;

public class ShoppingcartManager {
    
    private Shoppingcart shoppingcart;

    public ShoppingcartManager() {
        shoppingcart = new Shoppingcart();
    }

    public HashMap<Article, Integer> getArticlelist() {
        return shoppingcart.getArticlelist();
    }

    public float getShoppingcartsum() {
        return shoppingcart.getShoppingcartsum();
    }

    public void addArticle(Article article) {
        shoppingcart.addArticle(article);
    }

    public double checkout() {
        return shoppingcart.checkout();
    }

    public HashMap<Article, Integer> cancel() {
        return shoppingcart.cancel();
    }
}