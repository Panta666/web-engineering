package webengShop.businesslogic;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import webengShop.transfer.Article;
import webengShop.access.ArticleDAO;
import webengShop.access.DAOFactory;

public class ArticleManager {

    private ArticleDAO articleDAO = null;

    public ArticleManager() {
        setDAO();
        
        articleDAO.add(new Article(0,"TestArtikel01",10,7));
        articleDAO.add(new Article(0,"TestArtikel02",20,6));
        articleDAO.add(new Article(0,"TestArtikel03",30,5));
        articleDAO.add(new Article(0,"TestArtikel04",40,4));
        articleDAO.add(new Article(0,"TestArtikel05",50,3));
        articleDAO.add(new Article(0,"TestArtikel06",60,2));
    }

    public void setDAO() {
        articleDAO = DAOFactory.getArticleDAO();
    }

    public Article getArticle(int articleID) {
        Article a = articleDAO.get(articleID);
        return a;
    }

    public List<Article> getArticleList() {
        return articleDAO.getAll();
    }

    public ArrayList<String> updateArticle(Article article) {
        ArrayList<String> errors = verifyArticle(article);
        if (errors.isEmpty()) {
            articleDAO.update(article);
        }
        return errors;
    }

    public Article takeArticle(int articleID, int quantity) {
        Article a = articleDAO.get(articleID);
        if (quantity > a.getQuantity()) {
            a = null;
        } else {
            updateAmmount(a, -quantity);
        }
        return a;
    }

    public void addArticle(HashMap<Article, Integer> articlelist){
        for(Article a: articlelist.keySet()) {
            updateAmmount(a, articlelist.get(a));
        }
    }

    public ArrayList<String> newArticle(Article a) {
        ArrayList<String> errors = verifyArticle(a);
        if(errors.isEmpty()){
            articleDAO.add(a);
        }
        return errors;
    }

    public void delteArticle(Article article) {
        articleDAO.delete(article);
    }

    private void updateAmmount(Article article, int Ammount) {
        article.setQuantity(article.getQuantity() + Ammount);
        articleDAO.update(article);
    }

    private ArrayList<String> verifyArticle(Article article) {
        ArrayList<String> errors = new ArrayList<String>();
            if(article.getName() == null || article.getName().equals("")) errors.add("Name darf nicht leer sein.");
            if(article.getPrice() < 0) errors.add("Preis darf nicht negativ sein.");
            if(article.getQuantity() < 0) errors.add("Menge darf nicht negativ sein.");
            if(article.getId() < 0) errors.add("ID sollte nicht negativ sein.");
        return errors;
    }
}