<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="webengShop.transfer.Article"%>
<jsp:useBean id="article" class="webengShop.transfer.Article" scope="session"/>

<!DOCTYPE html>
<html>
    <head>
        <title>Artikeldetails!</title>
    </head>
    <body>
        <b>Gew&auml;hlter Artikel: <jsp:getProperty name="article" property="name"/></b><br>
        Preis: <jsp:getProperty name="article" property="price"/><br>
        Anzahl: <jsp:getProperty name="article" property="quantity"/><br>
        ID: <jsp:getProperty name="article" property="id"/><br>
        
        <form action="<%=response.encodeURL("./Frontcontroller")%>">
        <input type="hidden" name="action" value="shoppingcart">
        <input type="hidden" name="id" value="<jsp:getProperty name='article' property='id'/>">
        <table>
            <tr>
                <td><input type="number" name="quantity" value="1"></td>
                <td><input type="submit" value="Zum Warenkorb hinzuf&uuml;gen"></td>
            </tr>
        </table>
        </form>
    </body>
</html>