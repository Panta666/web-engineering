<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="webengShop.transfer.Article" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Warenkrob</title>
        <% HashMap<Article, Integer> shoppingcart = (HashMap<Article, Integer>) request.getAttribute("shoppingcartlist");%>
        <% float sum = (float) request.getAttribute("shoppingcartSum");%>

        <b>In dem Warenkorb sind:<b><br>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Preis</th>
                    <th>Anzahl</th>
                </tr>
            </thead>
            <tbody>
                <% for(Article a: shoppingcart.keySet()) {%>
                    <tr>
                        <td><%= a.getName()%></td>
                        <td><%= a.getPrice()%></td>
                        <td><%= shoppingcart.get(a)%></td>
                    </tr>
                <%}%>
                <tr>
                    <td colspan= '3' style='text-align:right'><u>Summe: <%= sum%>&euro;</u></td>
                </tr>
            </tbody>
                
        </table>
        <a href="./Frontcontroller?action=checkout">Bezahlen</a>
    </head>
    <body>

    </body>
</html>