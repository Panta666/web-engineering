<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="webengShop.transfer.Article"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<jsp:useBean id="article" class="webengShop.transfer.Article" scope="request"/>

<!DOCTYPE html>
<html>
    <head>
        <title>Artikeldetails!</title>
    </head>
    <body>
        <% ArrayList<Article> articles =  (ArrayList) request.getAttribute("alist"); %>
        <% ArrayList<String> errors = (ArrayList) request.getAttribute("errorlist");%>
        
        <% for(Article a : articles) {%>
            <form action='./Frontcontroller'>
                <input type='hidden' name='action' value='update'>
                <input type='hidden' name='origid' value ='<%=a.getId()%>'>
                <input type='hidden' name='AUTHTOKEN' value='totalSicher'>
                <table>
                    <tr>
                        <td>ID: <input type='number' name='id' value='<%=a.getId()%>'></td>
                        <td>Name: <input type='text' name='name' value='<%=a.getName()%>'></td>
                        <td>Preis: <input type='number' name='price' value='<%=a.getPrice()%>'></td>
                        <td>Anzahl: <input type='number' name='quantity' value='<%=a.getQuantity()%>'></td>
                        <td><input type='submit' name='sub' value='Entfernen'><input type='submit' name='sub' value='Aendern'></td>
                    </tr>
                <table>
            </form>
        <%} %>

        <form action='./Frontcontroller'>
            <table>
                <tr>
                <input type='hidden' name='action' value='add'>
                <input type='hidden' name='AUTHTOKEN' value='totalSicher'>
                    <td>ID: <input type='number' name='id' ></td>
                    <td>Name: <input type='text' name='name' ></td>
                    <td>Preis: <input type='number' name='price'></td>
                    <td>Anzahl: <input type='number' name='quantity' ></td>
                    <td><input type='submit' value='Hinzuf&uuml;gen'></td>
                </tr>
            <table>
            </form>
        <% if(errors != null && !errors.isEmpty()) {
            for(String error : errors) {
                out.println(error);
            }
        };%>
    </body>
</html>