<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@ page import="java.lang.*"%>
<!DOCTYPE html>
<html>
    <head>

    </head>
    <body>
        <h1>&Uuml;bungen P5 - Aufgabe 2.1</h1>
        <br>
        &#09;&#09;<b>a)</b>
        <br>
        &#09;&#09;Aktuelles Datum: <%= new java.text.SimpleDateFormat("dd.MM.YYYY").format(new java.util.Date())%>
        <br>
        &#09;&#09;3 x 5 = <%= 3*5%>
        <br><br>
        <b>b) Passwort generierung</b>
        <form action="test.jsp" method="POST">
            <table>
                <thead>
                    <tr>
                        <th>Passwort generator</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Codewort</td>
                        <td><input type="text" name="codeword" id="codeword"></td>
                    </tr>
                    <tr>
                        <td>Verschiebung</td>
                        <td><input type="number" name="skip" id ="skip"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="generieren"></td>
                    </tr>
                </tbody>
            </table>
        </form>
        <%!
            private String generatePasswort(String codeword, String skip) {
                String returnvalue= "";
                if(codeword != null && codeword.length() != 0 && skip != null && skip.length() != 0) {
                    for( int i = 0; i < codeword.length(); i++){
                        char letter = codeword.charAt(i);
                        returnvalue += (char) ( letter + Integer.valueOf(skip) % 128);
                    }
                } else {
                    if(codeword != null && codeword.length() == 0){
                        returnvalue = "Das Feld Codewort darf nicht leer sein!";
                    }
                    if (skip != null && skip.length() == 0) {
                        returnvalue = (returnvalue == "")? "Das Feld Verschiebung darf nicht leer sein!" : returnvalue + "<br>Das Fehld Verschiebung darf nicht leer sein!";
                    }
                }
                return returnvalue;
            }
        %>
        <%= generatePasswort(request.getParameter("codeword"), request.getParameter("skip")) %>
        <br><br>
        <b>c) Implizite Objekte</b>
        <br>
        HTTP-Methode: <%= request.getMethod() %>
        <br>
        Paramter aus DD: <%= pageContext.getServletContext().getInitParameter("Gruppenname") %>
    </body>
</html>