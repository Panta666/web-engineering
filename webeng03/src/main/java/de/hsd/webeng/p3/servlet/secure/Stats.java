package de.hsd.webeng.p3.servlet.secure;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.hsd.webeng.p3.listener.Usercounter;

/**
 * Servlet implementation class Stats
 */
@WebServlet(urlPatterns ={ "/secure/Stats" })
public class Stats extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Stats() {
        super();
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();

        int counter = Usercounter.getUsersLoggedin();

        String username = (String) request.getSession().getAttribute("username");

		String refurl = request.getHeader("referer");
		
		if(refurl == null) {
			refurl = "http://localhost:8080/index.html";
		}

        out.println("<!DOCTYPE html>");
        out.println("<html><head><title>Statistiken</title><body>");
        out.println("<h1>Statistiken &uumlber eingeloggte Benuzter</h1></br>");
        out.println("Aktuell eingeloggte Benutzer: " + counter + "</br>");
        out.println("Hallo " + username + "</br>");
        out.println("<a href='" + refurl + "'>Zur&uumlck zum Ursprung</a></br>");
        out.println("</body></html>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Implemented destroy for clean closing of the Server.
	 * Calls the super.destory().
	 */
	@Override
	public void destroy() {
		super.destroy();
	}


}
