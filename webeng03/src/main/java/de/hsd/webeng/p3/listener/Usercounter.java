package de.hsd.webeng.p3.listener;

import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class Usercounter implements HttpSessionListener {

    private static AtomicInteger counter = new AtomicInteger(0);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        synchronized (counter) {
            counter.incrementAndGet();
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        synchronized (counter) {
            counter.decrementAndGet();
        }

    }

    /**
     * Retunrs how many sessions are available on the Container.
     * 
     * @return Amount of users that are logged in.
     */
    public static int getUsersLoggedin() {
        return counter.get();
    }
    
}