package de.hsd.webeng.p3.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns ={ "/LoginServlet" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("user");

		if(username != null && userAccepted(username)) {
			
			HttpSession session2 = request.getSession();
			session2.setAttribute("username", username);
			response.encodeRedirectURL("./secure/index.html");
			response.sendRedirect("./secure/index.html");
		} else {
			// 401
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);

			// 403
			//response.sendError(HttpServletResponse.SC_FORBIDDEN);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * Implemented destroy for clean closing of the Server.
	 * Calls the super.destory().
	 */
	@Override
	public void destroy() {
		super.destroy();
	}

	/**
	 * Checks if the username is accepted.
	 * @param username Name to check.
	 * @return True if the username is 'admin'.
	 */
	private boolean userAccepted(String username) {
		boolean isAccepted = false;
		if(username.equals("admin")) {
			isAccepted = true;
		}

		return isAccepted;
	}

}
