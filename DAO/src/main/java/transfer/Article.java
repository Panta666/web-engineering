package transfer;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.ebean.annotation.Length;
import io.ebean.annotation.NotNull;

@Entity
@Table(name = "articles")
public class Article {
	
	@NotNull @Length(30)
	String name;
	float price;
	int quantity;
	@Id
	long id;
	
	public Article(long id, String name, float price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	public Article(long id, String name, float price, int quantity) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
	public Article() {
		// TODO Auto-generated constructor stub
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
